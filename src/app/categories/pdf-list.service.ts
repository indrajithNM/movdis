import { Injectable } from '@angular/core';
import { Pdf } from './pdf'

@Injectable({
  providedIn: 'root'
})
export class PdfListService {
  private pdfs:any[]=[
    {
      id:"1",
      draft:false,
      name:"Definitions-original",
      category:"Definitions of common phenomenology",
      path:"/assets/Definitions-original.pdf"
    },
    {
      id:"2",
      draft:false,
      name:"Definitions v.1",
      category:"Neuroanatomy and neurophysiology of movement disorders",
      path:"/assets/Definitions v.1.pdf"
    },
    {
      id:"3",
      draft:false,
      category:"Synucleinopathies"
    },
    {
      id:"3a",
      draft:false,
      name:"Definitions v.2",
      category:"Synucleinopathies",
      path:"/assets/Definitions v.2.pdf"
    },
    {
      id:"3b",
      draft:false,
      name:"Definitions v.3",
      category:"Synucleinopathies",
      path:"/assets/Definitions v.3.pdf"
    },
    {
      id:"4",
      draft:false,
      category:"Tauopathies"
    },
    {
      id:"4a",
      draft:false,
      name:"Progressive supra-nuclear palsy",
      category:"Tauopathies",
      path:"/assets/table of contents_v1.0.pdf"
    },
    {
      id:"4b",
      draft:false,
      name:"Cortico-basal syndrome",
      category:"Tauopathies",
      path:"/assets/table of contents_v1.0.pdf"
    },
    {
      id:"4c",
      draft:false,
      name:"Frontotemporal dementia-amyotrophic lateral sclerosis-parkinsonism",
      category:"Tauopathies",
      path:"/assets/table of contents_v1.0.pdf"
    },
    {
      id:"5",
      draft:false,
      name:"Huntington’s disease",
      category:"Huntington’s disease",
      path:"/assets/table of contents_v1.0.pdf"
    },
    {
      id:"6",
      draft:false,
      name:"Ataxia",
      category:"Ataxia",
    },
    {
      id:"7",
      draft:false,
      name:"Chorea",
      category:"Chorea",
      path:"/assets/table of contents_v1.0.pdf"
    },
    {
      id:"8",
      draft:false,
      name:"Dystonia",
      category:"Dystonia",
      path:"/assets/table of contents_v1.0.pdf"
    },
    {
      id:"9",
      draft:false,
      name:"Myoclonus",
      category:"Myoclonus",
      path:"/assets/table of contents_v1.0.pdf"
    },
    {
      id:"10",
      draft:false,
      name:"Tics",
      category:"Tics",
      path:"/assets/table of contents_v1.0.pdf"
    },
    {
      id:"11",
      draft:false,
      name:"Tremor",
      category:"Tremor",
      path:"/assets/table of contents_v1.0.pdf"
    },
    {
      id:"12",
      draft:false,
      name:"Miscellaneous movement disorders",
      category:"Miscellaneous movement disorders",
      path:"/assets/table of contents_v1.0.pdf"
    },
    {
      id:"6a",
      draft:false,
      name:"Definition",
      category:"Ataxia",
      prev:"/categories/sub/Ataxia",
      path:"/assets/ataxia/Definition.pdf"
    },
    {
      id:"6b",
      draft:false,
      name:"Highlights of cerebellar anatomy and corresponding clinical signs",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxia/Highlights of cerebellar anatomy and corresponding clinical signs.pdf"
    },
    {
      id:"6c",
      draft:false,
      name:"Initial investigations",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxia/Initial Investigations.pdf"
    },
    {
      id:"6d",
      draft:false,
      name:"Common differential diagnosis",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxia/Common Differential Diagnosis.pdf"
    },
    {
      id:"6e",
      draft:false,
      name:"Treatment options for ataxia",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxia/Treatment Options for Ataxia.pdf"
    },
    {
      id:"6f",
      draft:false,
      name:"Classification- Pediatric ataxia",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxia/Classification - Pediatric Ataxia.pdf"
    },
    {
      id:"6g",
      draft:false,
      name:"Classification- Adult ataxia",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxia/Classification - Adult.pdf"
    },
    {
      id:"6h",
      draft:false,
      name:"Acquired causes of ataxia",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxia/Aquired Cases of Ataxia.pdf"
    },
    {
      id:"6i",
      draft:false,
      name:"Inherited causes of ataxia – AR",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxia/ATAXIA FINAL EDITED.pdf"
    },
    {
      id:"6j",
      draft:false,
      name:"Inherited causes of ataxia – X-linked",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxia/Inherited Causes of Ataxia-X-Linked.pdf"
    },
    {
      id:"6k",
      draft:false,
      name:"Inherited causes of ataxia – Mitochondrial",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxia/Inherited Causes of Ataxia-Mitochondrial.pdf"
    },
    {
      id:"6l",
      draft:false,
      name:"Inherited causes of ataxia – AD",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxia/Inherited Causes of Ataxia - Autosomal Dominant.pdf"
    },
    {
      id:"6m",
      draft:false,
      name:"SCA3 Phenotypes",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxia/SCA3 Phenotypes.pdf"
    },
    {
      id:"6n",
      draft:false,
      name:"Metabolic disorders causing childhood ataxia",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxia/Metabolic DIsorders Causing Childhood Ataxia.pdf"
    },
    {
      id:"6o",
      draft:false,
      name:"Adult onset sporadic ataxia",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxiaAdult Onset Sporadic Ataxias.pdf"
    },
    {
      id:"6p",
      draft:false,
      name:"Spastic ataxia",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxia/Spastic Ataxia.pdf"
    },
    {
      id:"6q",
      draft:false,
      name:"Episodic/Intermittent ataxia",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxia/Episodic Intermittent Ataxia.pdf"
    },
    {
      id:"6r",
      draft:false,
      name:"Clinical pearls",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxia/Clinical Pearls.pdf"
    },
    {
      id:"6s",
      draft:false,
      name:"Radiological pearls",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxia/Radiological Pearls.pdf"
    },
    {
      id:"6t",
      draft:false,
      name:"Clinical cases",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxia/Clinical Cases.pdf"
    },
    {
      id:"6u",
      draft:false,
      name:"For further reading",
      prev:"/categories/sub/Ataxia",
      category:"Ataxia",
      path:"/assets/ataxia/For Further Reading.pdf"
    },
  ]

  constructor() { }

  getAllpdfs(){
    return [...this.pdfs]
  }

  getPdf(pdfId:string){
    return {...this.pdfs.find(pdf=>{
      return pdf.id==pdfId;
    })};
  }

  getSubList(category:string){
    return [...this.pdfs.filter((pdf)=>pdf.category==category && pdf.name!=category)];
  }
}
