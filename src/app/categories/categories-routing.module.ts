import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoriesPage } from './categories.page';

const routes: Routes = [
  {
    path: '',
    component: CategoriesPage
  },
  {
    path: ':pdfId',
    loadChildren: () => import('./pdfview/pdfview.module').then( m => m.PdfviewPageModule)
  },
  {
    path: 'sub/:subCat',
    loadChildren: () => import('./sublist/sublist.module').then( m => m.SublistPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoriesPageRoutingModule {}
