import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SublistPage } from './sublist.page';

const routes: Routes = [
  {
    path: '',
    component: SublistPage
  },
  {
    path: ':pdfId',
    loadChildren: () => import('../pdfview/pdfview.module').then( m => m.PdfviewPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SublistPageRoutingModule {}
