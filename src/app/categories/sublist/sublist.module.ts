import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SublistPageRoutingModule } from './sublist-routing.module';

import { SublistPage } from './sublist.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SublistPageRoutingModule
  ],
  declarations: [SublistPage]
})
export class SublistPageModule {}
