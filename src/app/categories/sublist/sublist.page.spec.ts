import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SublistPage } from './sublist.page';

describe('SublistPage', () => {
  let component: SublistPage;
  let fixture: ComponentFixture<SublistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SublistPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SublistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
