import { Component, OnInit } from '@angular/core';
import { PdfListService } from '../pdf-list.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-sublist',
  templateUrl: './sublist.page.html',
  styleUrls: ['./sublist.page.scss'],
})
export class SublistPage implements OnInit {
  backButtonSub:any;
  pdfList:any[];
  visibleList:any[];

  constructor( private activateRoute:ActivatedRoute,
    private pdfService:PdfListService,
    private router:Router,
    private platform:Platform) { }

  ngOnInit() {
    this.backButtonSub=  this.platform.backButton.subscribeWithPriority(1, ()=>{
      this.router.navigate(['/categories']);
    });
    this.activateRoute.paramMap.subscribe(paramMap=>{
      if (!paramMap.has('subCat')) {
        this.router.navigate(['/categories']);
        return;
      }
      const subCat=paramMap.get('subCat');
      this.pdfList= this.pdfService.getSubList(subCat);
      this.visibleList=this.pdfList.filter((pdf)=>pdf.draft==false)
    })
  }

  ngOnDestroy(){
    this.searchItems(null)
    this.backButtonSub.unsubscribe();
  }

  searchItems(event){
    let searchStr="";
    if (event!=null) {
      searchStr=event.target.value.toLowerCase().trim(); 
    }
    this.pdfList.forEach(elem => {
      if (elem.name && elem.name.toLowerCase().indexOf(searchStr) > -1) {
        elem.draft=false;
        // this.pdfList.forEach(el=>{
        //   if (!el.name&& el.category==elem.category) {
        //     el.draft=false;
        //   }
        // })
      }
      else{
        elem.draft=true;
      }
    });
    this.visibleList=this.pdfList.filter((pdf)=>pdf.draft==false)

  //   this.pdfList.forEach(elem =>{
  //     if(!elem.name && elem.category.toLowerCase().indexOf(searchStr)>-1){
  //       elem.draft=false;
  //       this.pdfList.forEach(el=>{
  //         if (el.category==elem.category) {
  //           el.draft=false;
  //         }
  //       })
  //     }
  //   })
  }

}
