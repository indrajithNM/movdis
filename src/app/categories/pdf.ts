export interface Pdf {
    id:string,
    group:boolean,
    name:string,
    category:string,
    path:string
}
