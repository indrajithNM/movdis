import { TestBed } from '@angular/core/testing';

import { PdfListService } from './pdf-list.service';

describe('PdfListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PdfListService = TestBed.get(PdfListService);
    expect(service).toBeTruthy();
  });
});
