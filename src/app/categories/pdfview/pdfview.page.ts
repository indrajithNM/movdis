import { Component, OnInit } from '@angular/core';
import { Pdf } from '../pdf';
import { ActivatedRoute, Router } from '@angular/router';
import { PdfListService } from '../pdf-list.service';
import { Platform } from '@ionic/angular';

const ZOOM_STEP:number = 0.25;
const DEFAULT_ZOOM:number = 1;

@Component({
  selector: 'app-pdfview',
  templateUrl: './pdfview.page.html',
  styleUrls: ['./pdfview.page.scss'],
})
export class PdfviewPage implements OnInit {
  pdf:any;
  public pdfZoom:number = DEFAULT_ZOOM;


  constructor( private activateRoute:ActivatedRoute,
    private pdfService:PdfListService,
    private router:Router,
    private platform:Platform) { }

  ngOnInit() {
    this.activateRoute.paramMap.subscribe(paramMap=>{
      if (!paramMap.has('pdfId')) {
        this.router.navigate(['/categories']);
        return;
      }
      const pdfId=paramMap.get('pdfId');
      this.pdf= this.pdfService.getPdf(pdfId);
    })
  }

  public zoomIn()
	{
    this.pdfZoom += ZOOM_STEP;
	}

	public zoomOut()
	{
		if (this.pdfZoom > DEFAULT_ZOOM) {
      this.pdfZoom -= ZOOM_STEP;
		}
	}

	public resetZoom()
	{
    this.pdfZoom = DEFAULT_ZOOM;
	}

}
