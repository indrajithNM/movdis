import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PdfviewPage } from './pdfview.page';

const routes: Routes = [
  {
    path: '',
    component: PdfviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PdfviewPageRoutingModule {}
