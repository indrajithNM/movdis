import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PdfviewPage } from './pdfview.page';

describe('PdfviewPage', () => {
  let component: PdfviewPage;
  let fixture: ComponentFixture<PdfviewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdfviewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PdfviewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
