import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PdfviewPageRoutingModule } from './pdfview-routing.module';

import { PdfviewPage } from './pdfview.page';

import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PdfviewPageRoutingModule,
    PdfViewerModule
  ],
  declarations: [PdfviewPage]
})
export class PdfviewPageModule {}
