import { Component, OnInit } from '@angular/core';
import { Pdf } from './pdf';
import { PdfListService } from './pdf-list.service';
import { Platform, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.page.html',
  styleUrls: ['./categories.page.scss'],
})
export class CategoriesPage implements OnInit {
  pdfList:any[];

  constructor(private pdfService:PdfListService,
    private platform:Platform,
    private router:Router,
    private alertController:AlertController) { }

  ngOnInit() {
    this.pdfList=this.pdfService.getAllpdfs();
  }

  ngOnDestroy(){
    this.searchItems(null)
  }

  async exitAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Do you really want to exit?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: 'Yes',
          handler: () => {
            navigator['app'].exitApp();
          }
        }
      ]
    });

    await alert.present();
  }

  searchItems(event){
    let searchStr="";
    if (event!=null) {
      searchStr=event.target.value.toLowerCase().trim(); 
    }   
    this.pdfList.forEach(elem => {
      if (elem.name && elem.name.toLowerCase().indexOf(searchStr) > -1) {
        elem.draft=false;
        this.pdfList.forEach(el=>{
          if (!el.name&& el.category==elem.category) {
            el.draft=false;
          }
        })
      }
      else{
        elem.draft=true;
      }
    });
    this.pdfList.forEach(elem =>{
      if(!elem.name && elem.category.toLowerCase().indexOf(searchStr)>-1){
        elem.draft=false;
        this.pdfList.forEach(el=>{
          if (el.category==elem.category) {
            el.draft=false;
          }
        })
      }
    })
  }
}
